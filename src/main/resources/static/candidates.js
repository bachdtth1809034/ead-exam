var candidates = [];

function findCandidate(candidateId) {
    return candidates[findCandidateKey(candidateId)];
}

function findCandidateKey(candidateId) {
    for (var key = 0;key< candidates.length; key++){
        if (candidates[key].id == candidateId){
            return key;
        }
    }
}

var candidateService = {
    findAll(fn){
        axios
            .get('/api/v1/candidates')
            .then(response => fn(response))
            .catch(error => console.log(error))
    },

    findById(id , fn){
        axios
            .get('/api/v1/candidates' + id)
            .then(response => fn(response))
            .catch(error => console.log(error))
    },

    create(candidate, fn){
        axios
            .get('/api/v1/candidates',  candidate)
            .then(response => fn(response))
            .catch(error => console.log(error))
    },

    update(id,candidate,fn){
        axios
            .put('/api/v1/candidates'+ id ,  candidate)
            .then(response => fn(response))
            .catch(error => console.log(error))
    },

    deleteCandidate(id , fn){
        axios
            .delete('/api/v1/candidates' + id)
            .then(response => fn(response))
            .catch(error => console.log(error))
    }
}

var List = Vue.extend({
    template: '#candidate-list',
    data: function (){
        return {candidates: [],searchKey: ''};
    },
    computed: {
        filteredCandidates(){
            return this.candidates.filter((candidate)=>{
                return candidate.name.indexOf(this.searchKey) > -1
                || candidate.birthday.indexOf(this.searchKey) > -1
                || candidate.address.indexOf(this.searchKey) > -1
            })
        }
    },
    mounted() {
        candidateService.findAll(r => {this.candidates = r.data; candidates = r.data})
    }
});

var Candidate = Vue.extend({
    template: '#candidate',
    data: function (){
        return {candidate: findCandidate(this.$route.params.candidate_id)};
    }
})

var CandidateEdit = Vue.extend({
    template: '#candidate-edit',
    data: function (){
        return {candidate: findCandidate(this.$route.params.candidate_id)};
    },
    methods:{
        updateCandidate: function (){
            candidateService.update(this.candidate.id, this.candidate, r=> router.push('/'))
        }
    }
});

var CandidateDelete = Vue.extend({
    template: '#candidate-delete',
    data: function (){
        return {candidate: findCandidate(this.$route.params.candidate_id)};
    },
    methods:{
        deleteCandidate: function (){
            candidateService.deleteCandidate(this.candidate.id, this.candidate, r=> router.push('/'))
        }
    }
});

var AddCandidate = Vue.extend({
    template: '#add-candidate',
    data(){
        return {
            candidate:{
                name: '', birthday: '', address: ''
            }
        };
    },
    methods:{
        createCandidate: function (){
            candidateService.create(this.candidate.id, this.candidate, r=> router.push('/'))
        }
    }
});

var router = new VueRouter({
    routes: [
        {path: '/',component: List},
        {path: '/candidate/:candidate_id',component: Candidate, name: 'candidate'},
        {path: '/add-candidate',component: AddCandidate},
        {path: '/candidate/:candidate_id/edit',component: CandidateEdit,name: 'candidate-edit'},
        {path: '/candidate/:candidate_id/delete',component: CandidateDelete,name: 'candidate-delete'},
    ]
});

new Vue({
    router
}).$mount('#app')
